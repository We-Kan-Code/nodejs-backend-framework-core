FROM node:lts-alpine
WORKDIR ./
RUN apk add --no-cache bash
RUN npm install -g pm2
COPY package.json package-lock.json ../
RUN npm install
COPY . ./
RUN npm run build
EXPOSE 9000
CMD [ "pm2-runtime", "start", "ecosystem.config.js" ]