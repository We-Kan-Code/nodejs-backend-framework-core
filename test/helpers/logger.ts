import bunyan from 'bunyan';

class testLogger {
  // private logger!: any;

  public static async init(
    name: string,
    streams: bunyan.Stream[],
    serializers: bunyan.Serializers,
  ) {}

  public constructor() {}

  public async trace(trace: any): Promise<void> {}

  public async debug(debug: any): Promise<void> {}

  public async info(info: any): Promise<void> {}

  public async warn(warning: any): Promise<void> {}

  public async error(error: any): Promise<void> {}

  public async fatal(fatal: any): Promise<void> {}

  public static async logErrorsWithFallback(
    log: any,
    prefix: string,
    error: any,
  ) {}
}

export default testLogger;
