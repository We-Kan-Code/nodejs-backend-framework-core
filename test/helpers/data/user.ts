import { User } from 'app/user/models/user';

const userId = '5d665175db0b753e7c1d599c';

const passwordHash =
  '12ab7797a6868cc8621f61f8efe98823116134c3a314505e3cdb157e9a84f4900695d58897a7d150cf487ecbe74818a1ed18d39c22218291e13ea632035f3387';

const literalTestUserSuccess = {
  firstName: 'John',
  lastName: 'Doe',
  email: 'johnd@gmail.com',
  profileImageUrl: 'https://aws.ibf.com/user/default.png',
  status: 0,
};

const userWithNoPasswords: InstanceType<typeof User> = new User();

userWithNoPasswords.firstName = literalTestUserSuccess.firstName;
userWithNoPasswords.lastName = literalTestUserSuccess.lastName;
userWithNoPasswords.email = literalTestUserSuccess.email;
userWithNoPasswords.profileImageUrl = literalTestUserSuccess.profileImageUrl;
userWithNoPasswords.status = literalTestUserSuccess.status;

export { userId, passwordHash, literalTestUserSuccess, userWithNoPasswords };
