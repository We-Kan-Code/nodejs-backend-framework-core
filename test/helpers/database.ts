import { userWithNoPasswords } from './data/user';

const mockFindOneAndUpdate = {
  exec: function() {
    return Promise.resolve(userWithNoPasswords);
  },
};

const mockFindOne = {
  select: function() {
    return this;
  },
  lean: function() {
    return this;
  },
  exec: function() {
    return Promise.resolve(userWithNoPasswords);
  },
};

const mockFindMany = {
  select: function() {
    return this;
  },
  lean: function() {
    return this;
  },
  exec: function() {
    return Promise.resolve([userWithNoPasswords, userWithNoPasswords]);
  },
};

const mockCountDocuments = {
  exec: function() {
    return Promise.resolve([userWithNoPasswords, userWithNoPasswords].length);
  },
};

const mockDeleteOne = {
  exec: function() {
    return Promise.resolve({ n: 1, ok: 1, deletedCount: 1 });
  },
};

export {
  mockFindOneAndUpdate,
  mockFindOne,
  mockFindMany,
  mockCountDocuments,
  mockDeleteOne,
};
