import { stub, assert } from 'sinon';
import { expect } from 'chai';
import dotenv from 'dotenv';
import configuration from 'configuration/manager';
import { SecretsManager } from 'configuration/classes/aws';
import firebase from 'firebase-admin';
import { Logger } from '@wekancompany/common';

let restore: any = [];

dotenv.config();

/** Configuration manager cases */
describe('Configuration manager', function() {
  it('should be a function (Classes are functions in JS)', function() {
    expect(configuration.init).to.be.an('function');
  });

  it('should have a static method create', function() {
    expect(configuration.isValid).to.be.an('function');
  });

  /** Configuration manager - All test cases */
  describe('the create method', function() {
    beforeEach(function() {
      /** Setup a sample config */
      restore['dotenv'] = stub(dotenv, 'config').resolves();
      restore['secretManager'] = stub(SecretsManager, 'getSecrets').resolves([
        'authentication secret',
        'sendgrid',
        'firebase',
      ]);
      restore['firebase'] = stub(firebase, 'initializeApp').resolves();
      restore['firebase']['cert'] = stub(
        firebase.credential,
        'cert',
      ).resolves();
    });

    afterEach(function() {
      restore['firebase']['cert'].restore();
      restore['firebase'].restore();
      restore['secretManager'].restore();
      restore['dotenv'].restore();
    });

    /** Configuration manager - Positive case 1 */
    it('should resolve successfully if the provided configuration is valid', async () => {
      await configuration.init();

      expect(configuration.authentication.token.name).to.be.a('string');
      expect(configuration.authentication.token.expiry).to.be.a('number');
      expect(configuration.authentication.token.secret).to.be.a('string');

      expect(configuration.aws.secrets.region).to.be.a('string');
      expect(configuration.aws.secrets.secretId).to.be.a('string');

      expect(configuration.cors.whitelist).to.be.a('array');

      expect(configuration.database.mongodb.host).to.be.a('string');
      expect(configuration.database.mongodb.database).to.be.a('string');

      expect(configuration.env).to.be.a('string');

      expect(configuration.firebaseCredentials).to.be.a('string');

      configuration.log.consoleLevel = configuration.log.fileLevel = <any>{
        [Symbol.toStringTag]: 'logLevelString',
      };

      configuration.log.consoleTo = <any>{
        [Symbol.toStringTag]: 'NodeJS.WriteStream',
      };

      configuration.log.logger = <any>{
        [Symbol.toStringTag]: 'Logger',
      };

      expect(configuration.log.consoleLevel).to.be.a('logLevelString');
      expect(configuration.log.consoleTo).to.be.a('NodeJS.WriteStream');
      expect(configuration.log.fileLevel).to.be.a('logLevelString');
      expect(configuration.log.fileTo).to.be.a('string');
      expect(configuration.log.logger).to.be.a('Logger');
    });

    /** Configuration manager - Negative cases */
    describe('error cases', function() {
      beforeEach(() => {
        restore['dotenv'].restore();
        restore['dotenv'] = stub(dotenv, 'config').resolves();
      });

      afterEach(function() {});

      /** Configuration manager - Negative case 1*/
      it('should throw an error when the configuration file is missing', async () => {
        restore['dotenv'].restore();
        restore['dotenv'] = stub(dotenv, 'config').returns({
          error: new Error('Cannot find configuration file'),
        });

        await configuration
          .init()
          .then()
          .catch((error) => {
            assert.calledOnce(restore['dotenv']);
            expect(error).to.not.be.undefined;
          });
        // restore['dotenv'].restore();
      });

      /** Configuration manager - Negative case 2*/
      it('should throw an error when the provided aws secrets region is empty', async () => {
        const tmp = process.env.AWS_SECRETS_REGION;
        process.env.AWS_SECRETS_REGION = '';

        await configuration
          .init()
          .then((result) => {})
          .catch((error) => {
            assert.calledOnce(restore['dotenv']);
            expect(error).to.not.be.undefined;
          });

        process.env.AWS_SECRETS_REGION = tmp;
        restore['dotenv'].restore();
      });

      /** Configuration manager - Negative case 3*/
      it('should throw an error when the provided aws secrets name is empty', async () => {
        const tmp = process.env.AWS_SECRETS_NAME;
        process.env.AWS_SECRETS_NAME = '';

        await configuration
          .init()
          .then((result) => {})
          .catch((error) => {
            assert.calledOnce(restore['dotenv']);
            expect(error).to.not.be.undefined;
          });

        process.env.AWS_SECRETS_NAME = tmp;
      });

      /** Configuration manager - Negative case 4*/
      it('should throw an error when the provided console log level is invalid', async () => {
        const tmp = process.env.LOG_CONSOLE_LEVEL;
        process.env.LOG_CONSOLE_LEVEL = 'over 9000';

        await configuration
          .init()
          .then((result) => {})
          .catch((error) => {
            assert.calledOnce(restore['dotenv']);
            expect(error).to.not.be.undefined;
          });

        process.env.LOG_CONSOLE_LEVEL = tmp;
      });

      /** Configuration manager - Negative case 5*/
      it('should throw an error when the provided file log level is invalid', async () => {
        const tmp = process.env.LOG_FILE_LEVEL;
        process.env.LOG_FILE_LEVEL = 'over 9000';

        await configuration
          .init()
          .then((result) => {})
          .catch((error) => {
            assert.calledOnce(restore['dotenv']);
            expect(error).to.not.be.undefined;
          });

        process.env.LOG_FILE_LEVEL = tmp;
      });

      /** Configuration manager - Negative case 6*/
      it('should throw an error when the bunyan logger fails to initialize', async () => {
        restore['logger'] = stub(Logger, 'init').returns(<any>'invalid logger');

        await configuration
          .init()
          .then((result) => {})
          .catch((error) => {
            assert.calledOnce(restore['dotenv']);
            expect(error).to.not.be.undefined;
          });

        restore['logger'].restore();
      });

      /** Configuration manager - Negative case 5*/
      it('should throw an error when the provided file log level is invalid', async () => {
        const tmp = process.env.FIREBASE_DATABASE;
        process.env.FIREBASE_DATABASE = '';

        await configuration
          .init()
          .then((result) => {})
          .catch((error) => {
            assert.calledOnce(restore['dotenv']);
            expect(error).to.not.be.undefined;
          });

        process.env.FIREBASE_DATABASE = tmp;
      });
    });
  });
});
