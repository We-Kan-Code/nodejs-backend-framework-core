import { stub, assert } from 'sinon';
import { expect } from 'chai';
import AuthService from 'app/auth/services/auth';
import UserService from 'app/user/services/user';
import jwt from 'jsonwebtoken';
import Messaging from 'common/messaging';
import {
  literalTestUserSuccess,
  userWithNoPasswords,
  passwordHash,
  userId,
} from 'test/helpers/data/user';
import bcrypt from 'bcrypt';
import mail from '@sendgrid/mail';
import configuration from 'configuration/manager';
import { Authentication, Token } from 'configuration/classes/authentication';
import { UserModel, User } from 'app/user/models/user';
import { InternalServerError } from '@wekancompany/common';
import { userStatus } from 'common/enums';
import { Sendgrid } from 'configuration/classes/messaging';
import { Log } from 'configuration/classes/logger';
import testLogger from 'test/helpers/logger';

let stubs: any = [];

/** Authentication test cases */
describe('Authentication service', function() {
  it('should be a function (Classes are functions in JS)', function() {
    expect(AuthService).to.be.an('function');
  });

  it('should have a static method generateTokens', function() {
    expect(typeof AuthService.generateTokens).to.equal('function');
  });

  it('should have a static method isEqual', function() {
    expect(typeof AuthService.isEqual).to.equal('function');
  });

  it('should have a static method authenticate', function() {
    expect(typeof AuthService.authenticate).to.equal('function');
  });

  it('should have a static method refresh', function() {
    expect(typeof AuthService.refresh).to.equal('function');
  });

  it('should have a static method resetPassword', function() {
    expect(typeof AuthService.resetPassword).to.equal('function');
  });

  it('should have a static method sendOtp', function() {
    expect(typeof AuthService.sendOtp).to.equal('function');
  });

  it('should have a static method verify', function() {
    expect(typeof AuthService.verify).to.equal('function');
  });

  /** Authentication generate tokens - All test cases */
  describe('the generate tokens method', function() {
    beforeEach(function() {
      stubs['user'] = UserModel.prototype;

      configuration.authentication = new Authentication();
      configuration.authentication.token = new Token();
      configuration.authentication.token.expiry = 30;
      configuration.authentication.token.secret = 'secret';

      stubs['jwt'] = stub(jwt, 'sign').resolves('signed token');
    });

    afterEach(function() {
      stubs['jwt'].restore();
    });

    afterEach(function() {
      delete userWithNoPasswords.password;
      delete userWithNoPasswords.tmpPassword;
    });

    /** Authentication generate tokens - Positive case 1 */
    it('should return an array constaining access and refresh tokens if tokens are signed successfully', async () => {
      const success = AuthService.generateTokens(literalTestUserSuccess.email);

      expect(success)
        .to.be.an('array')
        .of.length(2);
    });

    /** Authentication generate tokens - Negative cases */
    describe('error cases', function() {
      beforeEach(() => {});

      afterEach(function() {});

      /** Authentication generate tokens - Negative case 1*/
      it('should throw an internal server error if unable to sign tokens', async () => {
        stubs['jwt'].restore();
        stubs['jwt'] = stub(jwt, 'sign').callsFake(function() {
          throw new Error('error signing token');
        });

        expect(function() {
          AuthService.generateTokens(literalTestUserSuccess.email);
        }).to.throw(InternalServerError);

        stubs['jwt'].restore();
      });
    });
  });

  /** Authentication is equal - All test cases */
  describe('the is equal method', function() {
    beforeEach(function() {
      stubs['compare'] = stub(bcrypt, 'compare').resolves(true);
      stubs['generate'] = stub(AuthService, 'generateTokens').returns([
        'access token',
        'refresh token',
      ]);
      stubs['updateOne'] = stub(UserService, 'updateOne').resolves();
    });

    afterEach(function() {
      stubs['updateOne'].restore();
      stubs['generate'].restore();
      stubs['compare'].restore();
    });

    /** Authentication is equal - Positive case 1 */
    it('should return an array constaining access, refresh tokens and a verified status flag if the passwords match', async () => {
      userWithNoPasswords.password = passwordHash;
      const success = await AuthService.isEqual(
        userWithNoPasswords,
        userWithNoPasswords,
      );

      expect(success)
        .to.be.an('array')
        .of.length(3);

      assert.calledOnce(
        stubs['compare'].withArgs(
          userWithNoPasswords.password,
          userWithNoPasswords.password,
        ),
      );

      assert.calledOnce(stubs['generate'].withArgs(userWithNoPasswords.email));

      assert.calledOnce(stubs['updateOne']);
    });

    /** Authentication is equal - Positive case 2*/
    it('should return an array constaining access, refresh tokens and a verified status flag if the password matches the tmp password (password reset)', async () => {
      stubs['compare'].restore();
      stubs['compare'] = stub(bcrypt, 'compare');
      stubs['compare'].onCall(0).resolves(false);
      stubs['compare'].onCall(1).resolves(true);

      userWithNoPasswords.password = passwordHash;

      const tmp: InstanceType<typeof User> = new User();

      tmp.firstName = literalTestUserSuccess.firstName;
      tmp.lastName = literalTestUserSuccess.lastName;
      tmp.email = literalTestUserSuccess.email;
      tmp.profileImageUrl = literalTestUserSuccess.profileImageUrl;
      tmp.password = '123456789';
      tmp.tmpPassword = passwordHash;
      tmp.status = userStatus.notVerified;

      const success = await AuthService.isEqual(userWithNoPasswords, tmp);

      expect(success)
        .to.be.an('array')
        .of.length(3);

      assert.calledTwice(stubs['compare']);

      assert.calledOnce(stubs['generate'].withArgs(userWithNoPasswords.email));

      stubs['compare'].restore();
    });

    /** Authentication generate tokens - Negative cases */
    describe('error cases', function() {
      beforeEach(() => {});

      afterEach(function() {});

      /** Authentication is equal - Negative case 1*/
      it('should throw an unauthorized error if passwords do not match and tmpPassword is null', async () => {
        userWithNoPasswords.password = passwordHash;

        const tmp: InstanceType<typeof User> = new User();

        tmp.firstName = literalTestUserSuccess.firstName;
        tmp.lastName = literalTestUserSuccess.lastName;
        tmp.email = literalTestUserSuccess.email;
        tmp.profileImageUrl = literalTestUserSuccess.profileImageUrl;
        tmp.password = '123456789';
        tmp.tmpPassword = null;
        tmp.status = literalTestUserSuccess.status;

        stubs['compare'].restore();
        stubs['compare'] = stub(bcrypt, 'compare').resolves(false);

        await AuthService.isEqual(userWithNoPasswords, tmp)
          .then((result) => {})
          .catch((error) => {
            assert.calledOnce(
              stubs['compare'].withArgs(
                userWithNoPasswords.password,
                tmp.password,
              ),
            );
            error = {
              [Symbol.toStringTag]: 'UnauthorizedError',
            };
            expect(error).to.not.be.undefined;
            expect(error).to.be.an('UnauthorizedError');
          });

        stubs['compare'].restore();
      });

      /** Authentication is equal - Negative case 1*/
      it("should throw an unauthorized error if provided password does not match the user's password or tmp Password", async () => {
        userWithNoPasswords.password = passwordHash;

        const tmp: InstanceType<typeof User> = new User();

        tmp.firstName = literalTestUserSuccess.firstName;
        tmp.lastName = literalTestUserSuccess.lastName;
        tmp.email = literalTestUserSuccess.email;
        tmp.profileImageUrl = literalTestUserSuccess.profileImageUrl;
        tmp.password = '123456789';
        tmp.tmpPassword = '123456789';
        tmp.status = userStatus.verified;

        stubs['compare'].restore();
        stubs['compare'] = stub(bcrypt, 'compare').resolves(false);

        await AuthService.isEqual(userWithNoPasswords, tmp)
          .then((result) => {})
          .catch((error) => {
            assert.calledTwice(stubs['compare']);
            error = {
              [Symbol.toStringTag]: 'UnauthorizedError',
            };
            expect(error).to.not.be.undefined;
            expect(error).to.be.an('UnauthorizedError');
          });

        stubs['compare'].restore();
      });
    });
  });

  /** Authentication authenticate - All test cases */
  describe('the authenticate method', function() {
    const user: InstanceType<typeof User> = new User();

    beforeEach(function() {
      stubs['isEqual'] = stub(AuthService, 'isEqual').resolves([
        'access token',
        'refresh token',
        true,
      ]);

      user.firstName = literalTestUserSuccess.firstName;
      user.lastName = literalTestUserSuccess.lastName;
      user.email = literalTestUserSuccess.email;
      user.profileImageUrl = literalTestUserSuccess.profileImageUrl;
      user.password = '123456789';
      user.tmpPassword = '123456789';
      user.status = userStatus.verified;

      stubs['getOne'] = stub(UserService, 'getOne').resolves(user);
    });

    afterEach(function() {
      stubs['getOne'].restore();
      stubs['isEqual'].restore();
    });

    /** Authentication authenticate - Positive case 1 */
    it('should return an object constaining logged in user information along with access and refresh tokens', async () => {
      const success = await AuthService.authenticate(user);

      assert.calledOnce(stubs['isEqual']);
      assert.calledOnce(stubs['getOne']);
      expect(success.accessToken).to.not.be.undefined;
      expect(success.refreshToken).to.not.be.undefined;
    });

    /** Authentication authenticate - Negative cases */
    describe('error cases', function() {
      beforeEach(() => {});

      afterEach(function() {});

      /** Authentication authenticate - Negative case 1*/
      // it('', async () => {});
    });
  });

  /** Authentication refresh - All test cases */
  describe('the refresh method', function() {
    const user: InstanceType<typeof User> = new User();

    beforeEach(function() {
      stubs['generate'] = stub(AuthService, 'generateTokens').returns([
        'access token',
        'refresh token',
      ]);

      user.firstName = literalTestUserSuccess.firstName;
      user.lastName = literalTestUserSuccess.lastName;
      user.email = literalTestUserSuccess.email;
      user.profileImageUrl = literalTestUserSuccess.profileImageUrl;
      user.password = '123456789';
      user.tmpPassword = '123456789';
      user.status = userStatus.verified;

      stubs['getOne'] = stub(UserService, 'getOne').resolves(user);
      stubs['updateOne'] = stub(UserService, 'updateOne').resolves();
    });

    afterEach(function() {
      stubs['updateOne'].restore();
      stubs['getOne'].restore();
      stubs['generate'].restore();
    });

    /** Authentication refresh - Positive case 1 */
    it('should return an object constaining logged in user information along with access and refresh tokens', async () => {
      const success = await AuthService.refresh('refresh token');

      assert.calledOnce(stubs['getOne']);
      assert.calledOnce(stubs['generate']);
      assert.calledOnce(stubs['updateOne']);
      expect(success.accessToken).to.not.be.undefined;
      expect(success.refreshToken).to.not.be.undefined;
    });

    /** Authentication refresh - Negative cases */
    describe('error cases', function() {
      beforeEach(() => {});

      afterEach(function() {});

      /** Authentication refresh - Negative case 1*/
      // it('', async () => {});
    });
  });

  /** Authentication reset password - All test cases */
  describe('the reset password method', function() {
    const user: InstanceType<typeof User> = new User();

    beforeEach(function() {
      stubs['genSalt'] = stub(bcrypt, 'genSalt').resolves('$salt');
      stubs['hash'] = stub(bcrypt, 'hash').resolves('$hash');

      configuration.sendgrid = new Sendgrid();
      configuration.sendgrid.apiKey = '192830123';

      stubs['sendgridSetApi'] = stub(mail, 'setApiKey');
      stubs['sendMail'] = stub(mail, 'send').resolves();

      user.firstName = literalTestUserSuccess.firstName;
      user.lastName = literalTestUserSuccess.lastName;
      user.email = literalTestUserSuccess.email;
      user.profileImageUrl = literalTestUserSuccess.profileImageUrl;
      user.password = '123456789';
      user.tmpPassword = '123456789';
      user.status = userStatus.verified;

      stubs['getOne'] = stub(UserService, 'getOne').resolves(user);
      stubs['updateOne'] = stub(UserService, 'updateOne').resolves();
    });

    afterEach(function() {
      stubs['updateOne'].restore();
      stubs['getOne'].restore();
      stubs['sendMail'].restore();
      stubs['sendgridSetApi'].restore();
      stubs['hash'].restore();
      stubs['genSalt'].restore();
    });

    /** Authentication reset password - Positive case 1 */
    it('should send an email containing the temporary password', async () => {
      await AuthService.resetPassword(user.email);

      assert.calledOnce(stubs['getOne']);
      assert.calledOnce(stubs['genSalt']);
      assert.calledOnce(stubs['hash']);
      assert.calledOnce(stubs['sendgridSetApi']);
      assert.calledOnce(stubs['sendMail']);
      assert.calledOnce(stubs['updateOne']);
    });

    /** Authentication reset password - Negative cases */
    describe('error cases', function() {
      beforeEach(() => {});

      afterEach(function() {});

      /** Authentication reset password - Negative case 1*/
      // it('', async () => {});
    });
  });

  /** Authentication sent otp - All test cases */
  describe('the send otp method', function() {
    const user: InstanceType<typeof User> = new User();

    beforeEach(function() {
      user.firstName = literalTestUserSuccess.firstName;
      user.lastName = literalTestUserSuccess.lastName;
      user.email = literalTestUserSuccess.email;
      user.profileImageUrl = literalTestUserSuccess.profileImageUrl;
      user.password = '123456789';
      user.tmpPassword = '123456789';
      user.verificationCode = '0123';
      user.status = userStatus.notVerified;

      stubs['getOne'] = stub(UserService, 'getOne').resolves(user);
      stubs['verification'] = stub(
        Messaging.prototype,
        'sendVerificationMail',
      ).resolves();

      configuration.log = new Log();
      configuration.log.logger = <any>new testLogger();
    });

    afterEach(function() {
      stubs['verification'].restore();
      stubs['getOne'].restore();
    });

    /** Authentication sent otp - Positive case 1 */
    it('should send an email containing the verification code', async () => {
      await AuthService.sendOtp(userId);

      assert.calledOnce(stubs['getOne']);
      assert.calledOnce(stubs['verification']);
    });

    /** Authentication sent otp - Negative cases */
    describe('error cases', function() {
      beforeEach(() => {});

      afterEach(function() {});

      /** Authentication sent otp - Negative case 1*/
      // it('', async () => {});
    });
  });

  /** Authentication verify - All test cases */
  describe('the verify method', function() {
    const user: InstanceType<typeof User> = new User();

    beforeEach(function() {
      stubs['generate'] = stub(AuthService, 'generateTokens').returns([
        'access token',
        'refresh token',
      ]);

      user.firstName = literalTestUserSuccess.firstName;
      user.lastName = literalTestUserSuccess.lastName;
      user.email = literalTestUserSuccess.email;
      user.profileImageUrl = literalTestUserSuccess.profileImageUrl;
      user.password = '123456789';
      user.tmpPassword = '123456789';
      user.status = userStatus.notVerified;

      stubs['getOne'] = stub(UserService, 'getOne').resolves(user);
      stubs['updateOne'] = stub(UserService, 'updateOne').resolves();
    });

    afterEach(function() {
      stubs['updateOne'].restore();
      stubs['getOne'].restore();
      stubs['generate'].restore();
    });

    /** Authentication verify - Positive case 1 */
    it('should update the user status to verified if the verification code is correct', async () => {
      const success = await AuthService.verify(userId, '0123');

      assert.calledOnce(stubs['getOne']);
      assert.calledOnce(stubs['generate']);
      assert.calledOnce(stubs['updateOne']);
      expect(success.accessToken).to.not.be.undefined;
      expect(success.refreshToken).to.not.be.undefined;
    });

    /** Authentication verify - Negative cases */
    describe('error cases', function() {
      beforeEach(() => {});

      afterEach(function() {});

      /** Authentication verify - Negative case 1*/
      it('should throw an internal server error if unable to sign tokens', async () => {});
    });
  });
});
