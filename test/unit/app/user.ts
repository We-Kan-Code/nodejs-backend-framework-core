import { assert, match, stub, spy } from 'sinon';
import { expect } from 'chai';
import { Log } from 'configuration/classes/logger';
import { UserModel, User } from 'app/user/models/user';
import UserService from 'app/user/services/user';
import configuration from 'configuration/manager';
import Messaging from 'common/messaging';
import testLogger from 'test/helpers/logger';
import helpers from 'common/helpers';
import bcrypt from 'bcrypt';
import { userStatus } from 'common/enums';
import {
  literalTestUserSuccess,
  userWithNoPasswords,
  passwordHash,
  userId,
} from 'test/helpers/data/user';
import {
  mockFindOneAndUpdate,
  mockFindOne,
  mockCountDocuments,
  mockDeleteOne,
  mockFindMany,
} from 'test/helpers/database';

let stubs: any = [];

/** User test cases */
describe('User service', function() {
  it('should be a function (Classes are functions in JS)', function() {
    expect(UserService).to.be.an('function');
  });

  it('should have a static method create', function() {
    expect(typeof UserService.createOne).to.equal('function');
  });

  it('should have a static method updateOne', function() {
    expect(typeof UserService.updateOne).to.equal('function');
  });

  it('should have a static method getOne', function() {
    expect(typeof UserService.getOne).to.equal('function');
  });

  it('should have a static method getMany', function() {
    expect(typeof UserService.getMany).to.equal('function');
  });

  it('should have a static method deleteOne', function() {
    expect(typeof UserService.deleteOne).to.equal('function');
  });

  /** User create one - All test cases */
  describe('the create method', function() {
    const user = new User();

    beforeEach(function() {
      /** Setup a sample user to create */
      user.firstName = literalTestUserSuccess.firstName;
      user.lastName = literalTestUserSuccess.lastName;
      user.email = literalTestUserSuccess.email;
      user.password = passwordHash;
      user.profileImageUrl = literalTestUserSuccess.profileImageUrl;
      user.status = literalTestUserSuccess.status;

      stubs['user'] = UserModel.prototype;
      UserModel.prototype.save = function() {
        return literalTestUserSuccess;
      };

      stubs['user']['mail'] = stub(
        Messaging.prototype,
        'sendVerificationMail',
      ).resolves();

      configuration.log = new Log();
      configuration.log.logger = <any>new testLogger();

      stubs['genSalt'] = spy(bcrypt, 'genSalt');
      stubs['hash'] = spy(bcrypt, 'hash');
    });

    afterEach(function() {
      stubs['hash'].restore();
      stubs['genSalt'].restore();
      stubs['user']['mail'].restore();
      UserModel.prototype.save = stubs['user'];
    });

    /** User create one - Positive case 1 */
    it("should return object containing the unverified created user's information (Verification mail sent)", async () => {
      const padSpy = spy(helpers, 'pad');
      const getRandomSpy = spy(helpers, 'getRandom');

      await UserService.createOne(user);

      assert.calledOnce(getRandomSpy.withArgs(0, 10000));
      assert.calledOnce(padSpy.withArgs(match.string, '0', 4));
      assert.calledOnce(stubs['genSalt'].withArgs(match.number));
      assert.calledOnce(stubs['hash'].withArgs(match.string, match.string));
      assert.calledOnce(stubs['user']['mail']);

      getRandomSpy.restore();
      padSpy.restore();
    });

    /** User create one - Positive case 2 */
    it("should return object containing the verified created user's information (No verification mail sent)", async () => {
      const padSpy = spy(helpers, 'pad');
      const getRandomSpy = spy(helpers, 'getRandom');

      UserModel.prototype.save = function() {
        literalTestUserSuccess.status = userStatus.verified;
        return literalTestUserSuccess;
      };
      user.status = userStatus.verified;

      await UserService.createOne(user);

      assert.calledOnce(getRandomSpy.withArgs(0, 10000));
      assert.calledOnce(padSpy.withArgs(match.string, '0', 4));
      assert.calledOnce(stubs['genSalt'].withArgs(match.number));
      assert.calledOnce(stubs['hash'].withArgs(match.string, match.string));
      assert.callCount(stubs['user']['mail'], 0);

      user.status = userStatus.notVerified;

      getRandomSpy.restore();
      padSpy.restore();
    });

    /** User create one - Negative cases */
    describe('error cases', function() {
      let save!: any;

      beforeEach(() => {
        /** Setup a sample user to create */
        save = UserModel.prototype;
        UserModel.prototype.save = function() {
          return new Promise((resolve, reject) => {
            return reject(new Error('failed'));
          });
        };
      });

      afterEach(function() {
        UserModel.prototype.save = save;
      });

      /** User create one - Negative case 1*/
      it("should throw an error when the save database call fails to create the user's information", async () => {
        const padSpy = spy(helpers, 'pad');
        const getRandomSpy = spy(helpers, 'getRandom');

        await UserService.createOne(user)
          .then((result) => {})
          .catch((error) => {
            assert.calledOnce(getRandomSpy.withArgs(0, 10000));
            assert.calledOnce(padSpy.withArgs(match.string, '0', 4));
            assert.calledOnce(stubs['genSalt'].withArgs(match.number));
            assert.calledOnce(
              stubs['hash'].withArgs(match.string, match.string),
            );
            expect(error).to.not.be.undefined;
          });

        getRandomSpy.restore();
        padSpy.restore();
      });

      /** User create one - Negative case 2*/
      it('should throw an internal server error when the save database call fails due to server side issues (Network, Database connection down / timeouts)', async () => {
        UserModel.prototype.save = function() {
          return false;
        };

        const padSpy = spy(helpers, 'pad');
        const getRandomSpy = spy(helpers, 'getRandom');

        await UserService.createOne(user)
          .then((result) => {})
          .catch((error) => {
            assert.calledOnce(getRandomSpy.withArgs(0, 10000));
            assert.calledOnce(padSpy.withArgs(match.string, '0', 4));
            assert.calledOnce(stubs['genSalt'].withArgs(match.number));
            assert.calledOnce(
              stubs['hash'].withArgs(match.string, match.string),
            );
            error = {
              [Symbol.toStringTag]: 'InternalServerError',
            };
            expect(error).to.not.be.undefined;
            expect(error).to.be.an('InternalServerError');
          });

        getRandomSpy.restore();
        padSpy.restore();
      });

      /** User create one - Negative case 3*/
      it('should throw an error when bcrypt genSalt fails to generate the salt', async () => {
        const padSpy = spy(helpers, 'pad');
        const getRandomSpy = spy(helpers, 'getRandom');

        stubs['genSalt'].restore();
        stubs['hash'].restore();

        stubs['genSalt'] = stub(bcrypt, 'genSalt').rejects(new Error(''));

        await UserService.createOne(user)
          .then((result) => {})
          .catch((error) => {
            assert.calledOnce(getRandomSpy.withArgs(0, 10000));
            assert.calledOnce(padSpy.withArgs(match.string, '0', 4));
            assert.calledOnce(stubs['genSalt'].withArgs(match.number));
            expect(error).to.not.be.undefined;
            expect(error).to.be.an('error');
          });

        stubs['genSalt'].restore();
        getRandomSpy.restore();
        padSpy.restore();
      });

      /** User create one - Negative case 4*/
      it('should throw an error when bcrypt genSalt returns an empty or null salt', async () => {
        const padSpy = spy(helpers, 'pad');
        const getRandomSpy = spy(helpers, 'getRandom');

        stubs['genSalt'].restore();
        stubs['hash'].restore();

        stubs['genSalt'] = stub(bcrypt, 'genSalt').resolves(<any>false);

        await UserService.createOne(user)
          .then((result) => {})
          .catch((error) => {
            assert.calledOnce(getRandomSpy.withArgs(0, 10000));
            assert.calledOnce(padSpy.withArgs(match.string, '0', 4));
            assert.calledOnce(stubs['genSalt'].withArgs(match.number));
            error = {
              [Symbol.toStringTag]: 'InternalServerError',
            };
            expect(error).to.not.be.undefined;
            expect(error).to.be.an('InternalServerError');
          });

        stubs['genSalt'].restore();
        stubs['hash'].restore();
        getRandomSpy.restore();
        padSpy.restore();
      });

      /** User create one - Negative case 5*/
      it('should throw an error when bcrypt hash fails to hash the password', async () => {
        const padSpy = spy(helpers, 'pad');
        const getRandomSpy = spy(helpers, 'getRandom');

        stubs['genSalt'].restore();
        stubs['hash'].restore();

        stubs['genSalt'] = stub(bcrypt, 'genSalt').resolves('$salt');
        stubs['hash'] = stub(bcrypt, 'hash').rejects(new Error(''));

        await UserService.createOne(user)
          .then((result) => {})
          .catch((error) => {
            assert.calledOnce(getRandomSpy.withArgs(0, 10000));
            assert.calledOnce(padSpy.withArgs(match.string, '0', 4));
            assert.calledOnce(stubs['genSalt'].withArgs(match.number));
            assert.calledOnce(
              stubs['hash'].withArgs(match.string, match.string),
            );
            expect(error).to.not.be.undefined;
          });

        stubs['genSalt'].restore();
        stubs['hash'].restore();
        getRandomSpy.restore();
        padSpy.restore();
      });
    });
  });

  /** User update one - All test cases */
  describe('the update one method', function() {
    const user = new User();

    before(() => {
      user.firstName = 'Jane';
      user.lastName = 'Doe';
      user.password = passwordHash;
    });

    beforeEach(function() {
      stubs['updateOne'] = stub(UserModel, 'findOneAndUpdate').returns(
        mockFindOneAndUpdate as any,
      );
      stubs['genSalt'] = spy(bcrypt, 'genSalt');
      stubs['hash'] = spy(bcrypt, 'hash');
    });

    afterEach(function() {
      stubs['hash'].restore();
      stubs['genSalt'].restore();
      stubs['updateOne'].restore();
    });

    after(function() {});

    /** User update one - Positive case 1 */
    it("should return object containing the updated user's information (with user password)", async () => {
      const success = await UserService.updateOne({ _id: userId }, user);

      assert.calledOnce(stubs['genSalt'].withArgs(match.number));
      assert.calledOnce(stubs['hash'].withArgs(match.string, match.string));
      assert.calledOnce(stubs['updateOne'].withArgs({ _id: userId }, user));
      expect(success).to.be.equal(userWithNoPasswords);
    });

    /** User update one - Positive case 2 */
    it("should return object containing the updated user's information (without user password)", async () => {
      delete user.password;
      const success = await UserService.updateOne({ _id: userId }, user);

      assert.callCount(stubs['genSalt'].withArgs(match.number), 0);
      assert.callCount(stubs['hash'].withArgs(match.string, match.string), 0);
      assert.calledOnce(stubs['updateOne'].withArgs({ _id: userId }, user));
      const tmp = userWithNoPasswords;
      delete tmp.password;
      expect(success).to.be.equal(userWithNoPasswords);
    });

    /** User update one - Negative cases */
    describe('error cases', function() {
      beforeEach(function() {
        stubs['hash'].restore();
        stubs['genSalt'].restore();
        stubs['updateOne'].restore();

        stubs['updateOne'] = stub(UserModel, 'findOneAndUpdate').returns(
          mockFindOneAndUpdate as any,
        );
        stubs['genSalt'] = spy(bcrypt, 'genSalt');
        stubs['hash'] = spy(bcrypt, 'hash');
      });

      afterEach(function() {});

      /** User update one - Negative case 1 */
      it('should throw an error when the update database call fails', async () => {
        const tmp = mockFindOneAndUpdate;

        tmp.exec = <any>function() {
          return new Promise((resolve, reject) => {
            return reject(new Error('failed'));
          });
        };
        stubs['updateOne'].restore();
        stubs['updateOne'] = stub(UserModel, 'findOneAndUpdate').returns(
          tmp as any,
        );

        delete user.password;

        await UserService.updateOne({ _id: userId }, user)
          .then((result) => {})
          .catch((error) => {
            assert.calledOnce(
              stubs['updateOne'].withArgs({ _id: userId }, user),
            );
            expect(error).to.not.be.undefined;
          });

        user.password = passwordHash;
      });

      /** User update one - Negative case 2 */
      it('should throw an error when the update database call fails due to server side issues (Network, Database connection down / timeouts)', async () => {
        const tmp = mockFindOneAndUpdate;

        tmp.exec = <any>function() {
          return new Promise((resolve, reject) => {
            return resolve(false);
          });
        };
        stubs['updateOne'].restore();
        stubs['updateOne'] = stub(UserModel, 'findOneAndUpdate').returns(
          tmp as any,
        );

        delete user.password;

        await UserService.updateOne({ _id: userId }, user)
          .then((result) => {})
          .catch((error) => {
            assert.calledOnce(
              stubs['updateOne'].withArgs({ _id: userId }, user),
            );
            error = {
              [Symbol.toStringTag]: 'InternalServerError',
            };
            expect(error).to.not.be.undefined;
            expect(error).to.be.an('InternalServerError');
          });

        user.password = passwordHash;
      });

      /** User update one - Negative case 3 */
      it('should throw an error when the provided email address already exists', async () => {
        const tmp = mockFindOneAndUpdate;

        tmp.exec = <any>function() {
          return false;
        };
        stubs['updateOne'].restore();
        stubs['updateOne'] = stub(UserModel, 'findOneAndUpdate').returns(
          tmp as any,
        );

        delete user.password;

        await UserService.updateOne({ _id: userId }, user)
          .then((result) => {})
          .catch((error) => {
            assert.calledOnce(
              stubs['updateOne'].withArgs({ _id: userId }, user),
            );
            error = {
              [Symbol.toStringTag]: 'NotFoundError',
            };
            expect(error).to.not.be.undefined;
            expect(error).to.be.an('NotFoundError');
          });

        user.password = passwordHash;
      });

      /** User update one - Negative case 4 */
      it('should throw an error when bcrypt genSalt fails to generate the salt', async () => {
        stubs['genSalt'].restore();
        stubs['hash'].restore();

        stubs['genSalt'] = stub(bcrypt, 'genSalt').rejects(new Error(''));
        stubs['hash'] = stub(bcrypt, 'hash').resolves('$hash');

        user.password = passwordHash;

        await UserService.updateOne({ _id: userId }, user)
          .then((result) => {})
          .catch((error) => {
            assert.calledOnce(stubs['genSalt'].withArgs(match.number));
            assert.callCount(
              stubs['hash'].withArgs(match.string, match.string),
              0,
            );
            assert.callCount(
              stubs['updateOne'].withArgs({ _id: userId }, user),
              0,
            );
            expect(error).to.not.be.undefined;
            expect(error).to.be.an('error');
          });

        stubs['hash'].restore();
        stubs['genSalt'].restore();
      });

      /** User update one - Negative case 5 */
      it('should throw an error when bcrypt genSalt returns an empty or null salt', async () => {
        stubs['genSalt'].restore();
        stubs['hash'].restore();

        stubs['genSalt'] = stub(bcrypt, 'genSalt').resolves(<any>false);

        user.password = passwordHash;

        await UserService.updateOne({ _id: userId }, user)
          .then((result) => {})
          .catch((error) => {
            assert.calledOnce(stubs['genSalt'].withArgs(match.number));
            assert.callCount(
              stubs['hash'].withArgs(match.string, match.string),
              0,
            );
            assert.callCount(
              stubs['updateOne'].withArgs({ _id: userId }, user),
              0,
            );
            error = {
              [Symbol.toStringTag]: 'InternalServerError',
            };
            expect(error).to.not.be.undefined;
            expect(error).to.be.an('InternalServerError');
          });

        stubs['hash'].restore();
        stubs['genSalt'].restore();
      });

      /** User update one - Negative case 6 */
      it('should throw an error when bcrypt hash fails to hash the password', async () => {
        stubs['genSalt'].restore();
        stubs['hash'].restore();

        stubs['genSalt'] = stub(bcrypt, 'genSalt').resolves('$hash');
        stubs['hash'] = stub(bcrypt, 'hash').rejects(new Error(''));

        user.password = passwordHash;

        await UserService.updateOne({ _id: userId }, user)
          .then((result) => {})
          .catch((error) => {
            assert.calledOnce(stubs['genSalt'].withArgs(match.number));
            assert.calledOnce(
              stubs['hash'].withArgs(match.string, match.string),
            );
            assert.callCount(
              stubs['updateOne'].withArgs({ _id: userId }, user),
              0,
            );
            expect(error).to.not.be.undefined;
            expect(error).to.be.an('error');
          });

        stubs['hash'].restore();
        stubs['genSalt'].restore();
      });
    });
  });

  /** User get one - All test cases */
  describe('the get one method', function() {
    before(() => {
      stubs['getOne'] = stub(UserModel, 'findOne').returns(mockFindOne as any);
    });

    after(function() {
      stubs['getOne'].restore();
    });

    /** User get one - Positive case 1 */
    it("should return object containing the requested user's information", async () => {
      const success = await UserService.getOne({ _id: userId });

      assert.calledOnce(stubs['getOne'].withArgs({ _id: userId }));
      /** TODO: better result check */
      expect(success).to.include.keys(
        'firstName',
        'lastName',
        'email',
        'profileImageUrl',
        'status',
      );
    });

    /** User get one - Positive case 2 */
    it("should return object with the requested fields containing the requested user's information", async () => {
      stubs['getOne'].restore();
      mockFindOne.exec = <any>function() {
        return Promise.resolve({
          firstName: userWithNoPasswords.firstName,
          lastName: userWithNoPasswords.lastName,
          email: userWithNoPasswords.email,
        });
      };
      stubs['getOne'] = stub(UserModel, 'findOne').returns(mockFindOne as any);
      const fields = 'firstName, lastName, email'.replace(/,/g, ' ');

      const success = await UserService.getOne({ _id: userId }, fields);

      assert.calledOnce(stubs['getOne'].withArgs({ _id: userId }));
      expect(success).to.have.all.keys('firstName', 'lastName', 'email');

      stubs['getOne'].restore();
    });

    /** User get one - Negative cases */
    describe('error cases', function() {
      beforeEach(() => {
        stubs['getOne'].restore();

        mockFindOne.exec = <any>function() {
          return Promise.resolve(null);
        };

        stubs['getOne'] = stub(UserModel, 'findOne').returns(
          mockFindOne as any,
        );
      });

      afterEach(function() {});

      /** User get one - Negative case 1 */
      it("should return object containing the requested user's information", async () => {
        await UserService.getOne({ _id: userId })
          .then((result) => {})
          .catch((error) => {
            assert.calledOnce(stubs['getOne'].withArgs({ _id: userId }));
            error = {
              [Symbol.toStringTag]: 'InternalServerError',
            };
            expect(error).to.not.be.undefined;
            expect(error).to.be.an('InternalServerError');
          });
      });
    });
  });

  /** User get many - All test cases */
  describe('the get many method', function() {
    before(() => {
      stubs['getMany'] = stub(UserModel, 'find').returns(mockFindMany as any);
      stubs['countDocuments'] = stub(UserModel, 'countDocuments').returns(
        mockCountDocuments as any,
      );
    });

    after(function() {
      stubs['countDocuments'].restore();
      stubs['getMany'].restore();
    });

    /** User get many - Positive case 1 */
    it('should return array of objects containing information on all the users', async () => {
      const success = await UserService.getMany();

      assert.calledOnce(stubs['getMany'].withArgs({}));
      assert.calledOnce(stubs['countDocuments'].withArgs({}));

      for (let i = 0; i < success[0].length; i++) {
        expect(success[0][i].firstName).to.not.be.undefined;
        expect(success[0][i].lastName).to.not.be.undefined;
        expect(success[0][i].email).to.not.be.undefined;
        expect(success[0][i].profileImageUrl).to.not.be.undefined;
        expect(success[0][i].status).to.not.be.undefined;
      }
      expect(success[1]).to.not.be.undefined;
      expect(success[1]).to.be.a('number');
    });

    /** User get many - Negative test cases */
    describe('error cases', function() {
      beforeEach(() => {
        stubs['countDocuments'].restore();
        stubs['getMany'].restore();

        mockFindMany.exec = <any>function() {
          return Promise.reject(new Error(''));
        };
        mockCountDocuments.exec = <any>function() {
          return Promise.reject(new Error(''));
        };
        stubs['getMany'] = stub(UserModel, 'find').returns(mockFindMany as any);
        stubs['countDocuments'] = stub(UserModel, 'countDocuments').returns(
          mockCountDocuments as any,
        );
      });

      afterEach(function() {});

      /** User get many - Negative case 1 */
      it('should return array of objects containing information on all the users', async () => {
        await UserService.getMany()
          .then((result) => {})
          .catch((error) => {
            assert.calledOnce(stubs['getMany'].withArgs({}));
            assert.callCount(stubs['countDocuments'].withArgs({}), 0);
            expect(error).to.not.be.undefined;
            expect(error).to.be.an('error');
          });
      });

      /** User get many - Negative case 2 */
      it('should return array of objects with the requested fields containing information on all the users', async () => {
        stubs['getMany'].restore();
        stubs['countDocuments'].restore();

        mockFindMany.exec = <any>function() {
          return Promise.resolve([
            {
              firstName: userWithNoPasswords.firstName,
              lastName: userWithNoPasswords.lastName,
              email: userWithNoPasswords.email,
            },
          ]);
        };
        stubs['getMany'] = stub(UserModel, 'find').returns(mockFindMany as any);
        mockCountDocuments.exec = <any>function() {
          return Promise.resolve(1);
        };
        stubs['countDocuments'] = stub(UserModel, 'countDocuments').returns(
          mockCountDocuments as any,
        );

        const fields = 'firstName, lastName, email'.replace(/,/g, ' ');

        const success = await UserService.getMany({}, fields);

        assert.calledOnce(stubs['getMany'].withArgs({}, fields));

        for (let i = 0; i < success[0].length; i++) {
          expect(success[0][i].firstName).to.not.be.undefined;
          expect(success[0][i].lastName).to.not.be.undefined;
          expect(success[0][i].email).to.not.be.undefined;
        }
        expect(success[1]).to.not.be.undefined;
        expect(success[1]).to.be.a('number');

        stubs['countDocuments'].restore();
        stubs['getMany'].restore();
      });
    });
  });

  /** User delete one - All test cases */
  describe('the delete one method', function() {
    before(() => {
      stubs['deleteOne'] = stub(UserModel, 'deleteOne').returns(
        mockDeleteOne as any,
      );
    });

    after(function() {
      stubs['deleteOne'].restore();
    });

    /** User delete one - Positive case 1 */
    it('should return object containing status of a successful delete operation', async () => {
      const success = await UserService.deleteOne(userId);

      assert.calledOnce(stubs['deleteOne'].withArgs({ _id: userId }));
      expect(success).to.be.deep.equal({ n: 1, ok: 1, deletedCount: 1 });
    });

    /** User delete one - Negative test cases */
    describe('error cases', function() {
      before(() => {
        stubs['deleteOne'].restore();
      });

      afterEach(function() {
        stubs['deleteOne'].restore();
      });

      /** User delete one - Negative case 1 */
      it('should throw an error when the delete one database call fails', async () => {
        mockDeleteOne.exec = function() {
          return Promise.reject(new Error(''));
        };
        stubs['deleteOne'] = stub(UserModel, 'deleteOne').returns(
          mockDeleteOne as any,
        );
        await UserService.deleteOne(userId)
          .then((result) => {})
          .catch((error) => {
            assert.calledOnce(stubs['deleteOne'].withArgs({ _id: userId }));
            expect(error).to.not.be.undefined;
            expect(error).to.be.an('error');
          });
      });

      /** User delete one - Negative case 2 */
      it('should return object containing status of a unsuccessful delete operation', async () => {
        mockDeleteOne.exec = function() {
          return Promise.resolve({ n: 1, ok: 0, deletedCount: 0 });
        };
        stubs['deleteOne'] = stub(UserModel, 'deleteOne').returns(
          mockDeleteOne as any,
        );
        await UserService.deleteOne(userId)
          .then((result) => {})
          .catch((error) => {
            assert.calledOnce(stubs['deleteOne'].withArgs({ _id: userId }));
            error = {
              [Symbol.toStringTag]: 'InternalServerError',
            };
            expect(error).to.not.be.undefined;
            expect(error).to.be.an('InternalServerError');
          });
      });
    });
  });
});
