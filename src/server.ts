import 'module-alias/register';
import 'reflect-metadata';
import { Logger } from '@wekancompany/common';
import { messages, prefix } from 'resources/strings/server';
import configuration from 'configuration/manager';
import database from 'boot/database';
import server from 'boot/server';

let log: any = console;

configuration
  /**
   * Loads configuration information from local env
   * and fomr aws secrets
   **/
  .init()
  .then(
    /** Settings loaded successfully */
    (status: string): Promise<{}> => {
      log = configuration.log.logger;
      log.info(`${messages.boot.start} ${status}`);
      return configuration.isValid();
    },
  )
  .then(
    /** Settings are valid */
    (status: any): Promise<{}> => {
      log.info(`${messages.boot.settings} ${status}`);
      return database.connect();
    },
  )
  .then(
    /** Connected to the database */
    (status): Promise<{}> => {
      log.info(`${messages.boot.database} ${status}`);
      return server.start();
    },
  )
  .then((status): void => {
    /**
     * Server started on the configured port.
     * TODO: Run startup tasks here.
     **/
    log.info(`${messages.boot.server} ${configuration.port} ${status}`);
    log.info(`${messages.boot.success}`);
  })
  .catch((error): void => {
    Logger.logErrorsWithFallback(log, prefix, error);
    process.exit(0);
  });

process.on('uncaughtException', (error): void => {
  log.fatal(error);
  log.trace(error);
  process.exit(1);
});
