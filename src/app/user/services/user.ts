import { InternalServerError, NotFoundError } from '@wekancompany/common';
import { User, UserModel } from '../models/user';
import bcrypt from 'bcrypt';
import Messaging from 'common/messaging';
import { messages } from 'resources/strings/app/user';
import helpers from 'common/helpers';
import { deletedData } from 'common/custom/types';
import { userStatus } from 'common/enums';

class UserService {
  public static async createOne(user: User): Promise<any> {
    const random = helpers.getRandom(0, 10000);
    const verificationCode = helpers.pad(String(random), '0', 4);
    return bcrypt
      .genSalt(10)
      .then(
        (salt: any): Promise<string> => {
          if (!salt) {
            /** Shouldn't reach this, unless something is broken server side */
            throw new InternalServerError(messages.errors.create);
          }
          return bcrypt.hash(user.password, salt);
        },
      )
      .then(
        async (hash: string): Promise<User> => {
          const data = user;
          data.password = hash;
          data.verificationCode = verificationCode;
          return new UserModel(data).save();
        },
      )
      .then(
        (result: User): User => {
          if (!result) {
            throw new InternalServerError(messages.errors.create);
          } else {
            if (result.status === 0) {
              new Messaging().sendVerificationMail(
                result.email,
                verificationCode,
              );
            }
            return result;
          }
        },
      )
      .catch(
        (error: any): Promise<any> => {
          return Promise.reject(error);
        },
      );
  }

  public static async updateOne(
    condition: {},
    user: Partial<User>,
  ): Promise<any> {
    if (user.password !== undefined) {
      user.password = await bcrypt
        .genSalt(10)
        .then(
          (salt: string): Promise<string> => {
            if (!salt) {
              throw new InternalServerError(messages.errors.create);
            }
            return bcrypt.hash(user.password, salt);
          },
        )
        .catch((error: any): any => Promise.reject(error));
      user.status = userStatus.verified;
    }
    return UserModel.findOneAndUpdate(condition, user, {
      fields: {
        firstName: 1,
        lastName: 1,
        email: 1,
        profileImageUrl: 1,
        status: 1,
      },
      new: true,
    })
      .exec()
      .then((result: any): {} => {
        if (!result) {
          throw new NotFoundError(messages.errors.find);
        }
        return result;
      })
      .catch((error: any): any => Promise.reject(error));
  }

  public static async getOne(query?: {}, fields?: string): Promise<any> {
    const filter = query !== undefined ? query : {};
    return UserModel.findOne(filter)
      .select(
        fields !== undefined
          ? fields.replace(/,/g, ' ')
          : `firstName
						lastName
						email
						profileImageUrl
						status`,
      )
      .lean()
      .exec()
      .then((result: Partial<User>): Partial<User> | NotFoundError => {
        if (!result) {
          throw new NotFoundError(messages.errors.find);
        } else {
          return result;
        }
      })
      .catch((error: any): Promise<any> => Promise.reject(error));
  }

  public static async getMany(
    query?: {},
    fields?: string,
    sort?: string,
    offset?: number,
    limit?: number,
  ): Promise<any> {
    const filter = query !== undefined ? query : {};

    offset = offset === undefined && limit !== undefined ? 0 : offset;
    limit = limit === undefined && offset !== undefined ? 10 : limit;

    const res = sort !== undefined ? sort.split('|') : ['createdAt', 'desc'];

    let sortby: any = {};
    sortby[res[0]] = res[1];

    return UserModel.find(
      filter,
      fields !== undefined
        ? fields.replace(/,/g, ' ')
        : `firstName
					lastName
					email
					profileImageUrl
					status`,
      { skip: offset, limit, sort: sortby },
    )
      .lean()
      .exec()
      .then(
        async (result: User[]): Promise<[Partial<User>[], number]> => {
          const count = await UserModel.countDocuments(filter).exec();
          return [result, count];
        },
      )
      .catch(
        (error: any): Promise<any> => {
          return Promise.reject(error);
        },
      );
  }

  public static async deleteOne(id: string): Promise<any> {
    return UserModel.deleteOne({ _id: id })
      .exec()
      .then(
        (result: deletedData): deletedData => {
          if (result.deletedCount == 0)
            throw new NotFoundError(messages.errors.find);
          return result;
        },
      )
      .catch((error: any): Promise<any> => Promise.reject(error));
  }
}

export default UserService;
