import {
  Body,
  Controller,
  Get,
  Param,
  Patch,
  Post,
  QueryParam,
  Req,
  Res,
  UseBefore,
  Delete,
} from 'routing-controllers';
import { httpStatus } from '@wekancompany/common';
import { Response } from 'express';
import { User } from 'app/user/models/user';
import Authentication from 'middleware/authentication';
import UserService from 'app/user/services/user';
import Validator from 'middleware/validator';

@Controller('/users')
class UserController {
  @Post()
  @UseBefore(Validator.validate(User, ['create']))
  public async createUser(
    @Body() user: User,
    @Res() res: Response,
  ): Promise<any> {
    return UserService.createOne(user)
      .then(
        (result: any): Response => {
          return res
            .status(httpStatus.created)
            .json({ data: { user: { _id: result._id } } });
        },
      )
      .catch((error: any): any => Promise.reject(error));
  }

  @Patch()
  @UseBefore(Validator.validate(User, ['update']))
  @UseBefore(Authentication)
  public async updateUser(
    @Req() req: any,
    @Body() user: User,
    @Res() res: Response,
  ): Promise<any> {
    return UserService.updateOne({ email: req.decodedToken.email }, user)
      .then(
        (result: any): Response => {
          return res
            .status(httpStatus.created)
            .json({ data: { user: result } });
        },
      )
      .catch((error: any): any => Promise.reject(error));
  }

  @Get('/:id')
  @UseBefore(Authentication)
  public async getUser(
    @Param('id') id: string,
    @QueryParam('fields') fields: string,
    @Res() res: Response,
  ): Promise<any> {
    return UserService.getOne({ _id: id }, fields)
      .then(
        (result: any): Response => {
          return res
            .status(httpStatus.created)
            .json({ data: { user: result } });
        },
      )
      .catch((error: any): any => Promise.reject(error));
  }

  @Get()
  @UseBefore(Authentication)
  public async getUsers(
    @QueryParam('fields') fields: string,
    @QueryParam('sort') sort: string,
    @QueryParam('offset') offset: number,
    @QueryParam('limit') limit: number,
    @Res() res: Response,
  ): Promise<any> {
    return UserService.getMany({}, fields, sort, offset, limit)
      .then(
        (results: any): Response => {
          const pageNo = Math.round(offset / limit + 1);
          return res.status(httpStatus.created).json({
            data: { users: results[0] },
            meta: {
              pagination: {
                page: pageNo > results[1] ? results[1] : pageNo,
                total: results[1],
              },
            },
          });
        },
      )
      .catch((error: any): any => Promise.reject(error));
  }

  @Delete('/:id')
  @UseBefore(Authentication)
  public async deleteUser(
    @Param('id') id: string,
    @Req() req: any,
    @Res() res: Response,
  ): Promise<void> {
    return UserService.deleteOne(id)
      .then((result: any): void => {
        return res.status(httpStatus.noContent).end();
      })
      .catch((error: any): any => Promise.reject(error));
  }
}

export default UserController;
