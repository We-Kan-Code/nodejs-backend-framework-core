import { messages } from 'resources/strings/app/auth';
import { User } from 'app/user/models/user';
import { userStatus } from 'common/enums';
import bcrypt from 'bcrypt';
import configuration from 'configuration/manager';
import crypto from 'crypto';
import jwt from 'jsonwebtoken';
import UserService from 'app/user/services/user';
import {
  ApiError,
  UnauthorizedError,
  InternalServerError,
} from '@wekancompany/common';
import mail from '@sendgrid/mail';
import Messaging from 'common/messaging';

class AuthService {
  public static generateTokens(email: string) {
    try {
      const expiry =
        Date.now() / 1000 +
        configuration.authentication.token.expiry * 60 * 1000;
      const secret = configuration.authentication.token.secret;

      return [
        // Access token
        jwt.sign(
          JSON.stringify({
            email: email,
            exp: expiry,
          }),
          configuration.authentication.token.secret,
          { algorithm: 'HS512' },
        ),
        // Refresh token
        jwt.sign(JSON.stringify({ email }), secret, { algorithm: 'HS512' }),
      ];
    } catch (error) {
      throw new InternalServerError(messages.errors.generate.token.failed);
    }
  }

  public static async isEqual(user: User, savedUser: any): Promise<any> {
    return bcrypt
      .compare(user.password, savedUser.password)
      .then(
        async (result: any): Promise<any> => {
          if (result) {
            savedUser.tmpPassword = null;
            return true;
          } else {
            if (savedUser.tmpPassword === null)
              throw new UnauthorizedError(messages.errors.unauthorized);
            return bcrypt.compare(user.password, savedUser.tmpPassword);
          }
        },
      )
      .then(
        async (result: any): Promise<any> => {
          if (result) {
            const tokens = AuthService.generateTokens(user.email);

            UserService.updateOne(
              { _id: savedUser._id },
              {
                tmpPassword: null,
                refreshToken: tokens[1],
              },
            )
              .then((result) => {})
              .catch((error: any) => {
                // Log error
              });

            return [...tokens];
          }
          throw new UnauthorizedError(messages.errors.unauthorized);
        },
      )
      .catch((error: any): Promise<any> => Promise.reject(error));
  }

  public static async authenticate(user: User) {
    return UserService.getOne(
      {
        email: user.email,
        status: {
          $in: [
            userStatus.notVerified,
            userStatus.verified,
            userStatus.forceResetPasswordOnLogin,
          ],
        },
      },
      'firstName lastName email profileImageUrl password tmpPassword status',
    )
      .then(
        async (result: any): Promise<{}> => {
          const tokens: any = await AuthService.isEqual(user, result).catch(
            (error: any): Promise<any> => {
              throw new ApiError(error.status, error.message);
            },
          );
          result.accessToken = tokens[0];
          result.refreshToken = tokens[1];
          return result;
        },
      )
      .then(
        (result: any): Response => {
          delete result.password;
          delete result.tmpPassword;
          return result;
        },
      )
      .catch((error: any): Promise<any> => Promise.reject(error));
  }

  public static async refresh(refreshToken: string) {
    return UserService.getOne(
      { refreshToken },
      'firstName lastName email profileImageUrl password tmpPassword status',
    )
      .then(
        async (result: any): Promise<{}> => {
          const tokens = AuthService.generateTokens(result.email);
          UserService.updateOne({ _id: result._id }, { refreshToken });

          result.accessToken = tokens[0];
          result.refreshToken = tokens[1];

          delete result.password;
          delete result.tmpPassword;

          return result;
        },
      )
      .catch((error: any): Promise<any> => Promise.reject(error));
  }

  public static async resetPassword(email: string) {
    return UserService.getOne({
      email,
      status: {
        $in: [userStatus.verified, userStatus.forceResetPasswordOnLogin],
      },
    })
      .then(
        async (result: any): Promise<any> => {
          const tmpPassword = crypto.randomBytes(6).toString('hex');
          const hash = await bcrypt.genSalt(10).then(
            (salt: string): Promise<string> => {
              if (!salt) {
                throw new InternalServerError(
                  messages.errors.generate.salt.failed,
                );
              }
              return bcrypt.hash(
                crypto
                  .createHash('sha512')
                  .update(tmpPassword)
                  .digest('hex'),
                salt,
              );
            },
          );

          UserService.updateOne(
            { _id: result._id },
            {
              tmpPassword: hash,
              status: userStatus.forceResetPasswordOnLogin,
            },
          );

          return [result.email, tmpPassword];
        },
      )
      .then(
        async (result: any): Promise<void> => {
          mail.setApiKey(configuration.sendgrid.apiKey);
          const msg = {
            to: result[0],
            from: 'sanchanm@wekan.company',
            subject: 'Password reset',
            text: 'Your temporary password ' + result[1],
            html: 'Your temporary password <strong>' + result[1] + '</strong>',
          };
          mail.send(msg);
        },
      )
      .catch((error: any): Promise<any> => Promise.reject(error));
  }

  public static async sendOtp(id: string) {
    return UserService.getOne({ _id: id }, 'email verificationCode')
      .then(
        async (result: any): Promise<void> => {
          if (result.verificationCode !== null) {
            new Messaging().sendVerificationMail(
              result.email,
              result.verificationCode,
            );
          }
        },
      )
      .catch((error: any): Promise<any> => Promise.reject(error));
  }

  public static async verify(id: string, verificationCode: string) {
    return UserService.getOne(
      {
        _id: id,
        verificationCode,
        status: userStatus.notVerified,
      },
      'firstName lastName email profileImageUrl password tmpPassword status',
    )
      .then(
        async (result: any): Promise<{}> => {
          const tokens = AuthService.generateTokens(result.email);

          UserService.updateOne(
            { _id: result._id },
            {
              refreshToken: tokens[1],
              status: userStatus.verified,
            },
          )
            .then((result) => {})
            .catch((error: any) => {
              // Log error
            });

          result.accessToken = tokens[0];
          result.refreshToken = tokens[1];
          result.status = userStatus.verified;

          delete result.password;
          delete result.tmpPassword;

          return result;
        },
      )
      .catch((error: any): Promise<any> => Promise.reject(error));
  }
}

export default AuthService;
