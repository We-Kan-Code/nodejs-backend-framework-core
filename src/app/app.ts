import { useExpressServer } from 'routing-controllers';
import Authentication from 'middleware/authentication';
import bodyparser from 'body-parser';
import Cors from 'configuration/classes/cors';
import express from 'express';
import helmet from 'helmet';
import AuthhController from 'app/auth/controllers/auth';
import UserController from 'app/user/controllers/user';

class App {
  public server: any = null;
  private corsOptions = {};

  public constructor() {
    this.server = express();
    this.corsOptions = {
      origin: new Cors().corsHandler,
      credentials: true,
      exposedHeaders: ['Content-Length'],
    };
  }

  public async createServer(): Promise<void> {
    this.server.use(helmet());
    this.server.use(bodyparser.json());
    this.server.use(bodyparser.urlencoded({ extended: false }));

    new Authentication().init();

    useExpressServer(this.server, {
      cors: this.corsOptions,
      routePrefix: '/v1',
      defaultErrorHandler: false,
      validation: false,
      controllers: [AuthhController, UserController],
    });
  }
}

export default App;
