import {
  Middleware,
  ExpressErrorMiddlewareInterface,
} from 'routing-controllers';
import mongoose from 'mongoose';
import {
  ApiError,
  ConflictError,
  ValidationError,
  BadRequestError,
  httpStatus,
} from '@wekancompany/common';
import configuration from 'configuration/manager';

@Middleware({ type: 'after' })
class ErrorHandler implements ExpressErrorMiddlewareInterface {
  private readonly log = configuration.log.logger;

  public error(err: any, req: any, res: any, next: (error: any) => any): void {
    let error = err;

    /** TODO check this */
    if (error.statusCode !== undefined && error.message !== undefined)
      error = new ApiError(error.statusCode, error.message);

    /** Check for multer errors - Use if handling file uploads */
    // if (error.name !== undefined && error.name === 'MulterError')
    //   error = new BadRequestError(error.message);

    /** TODO isIn array of errors */
    if (error.httpCode !== undefined && error.name === 'ParamRequiredError') {
      error = new BadRequestError('Invalid parameters provided');
    }

    if (error.errors !== undefined && Object.keys(error.errors).length) {
      this.log.error(error.errors);
      this.log.trace(error.stack);
      res.status(error.status).send({ errors: { messages: error.errors } });
    } else {
      this.log.error(error.message);
      this.log.trace(error.stack);
      res
        .status(error.status ? error.status : httpStatus.internalServerError)
        .send({ errors: { messages: [error.message] } });
    }
  }

  public static mongo(model: string): any {
    return (
      error: any,
      doc: mongoose.Document,
      next: (err?: mongoose.NativeError) => void,
    ): void => {
      if (error.name === 'MongoError' && error.code === 11000) {
        return next(new ConflictError(`${model} already exists`));
      }

      if (error.name === 'ValidationError')
        return next(
          new ValidationError(
            `Please provide valid data for ${error.errors[Object.keys(error.errors)[0]].path}`,
          ),
        );

      if (error.name === 'CastError')
        return next(
          new ValidationError(
            `Please provide a valid ${String(
              error.kind,
            ).toLowerCase()} value for ${error.path}`,
          ),
        );

      return next(error);
    };
  }
}

export default ErrorHandler;
