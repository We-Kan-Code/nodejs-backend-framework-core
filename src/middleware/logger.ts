import { Middleware, ExpressMiddlewareInterface } from 'routing-controllers';
import configuration from 'configuration/manager';

@Middleware({ type: 'before', priority: 1 })
class LogRequest implements ExpressMiddlewareInterface {
  use(request: any, response: any, next: (error?: any) => any): void {
    const log = configuration.log.logger;
    const prefix = 'Request';
    log.info(`[${prefix}][${request.hostname}] ${request.originalUrl}`);
    next();
  }
}

export { LogRequest };
