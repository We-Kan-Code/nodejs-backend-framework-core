interface IDecodedToken {
  email: string;
  exp: number;
}

export { IDecodedToken };
