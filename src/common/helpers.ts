function getRandom(min: number, max: number): number {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
}

function pad(n: string, padWith: string, width: number): string {
  return n.length >= width
    ? n
    : new Array(width - n.length + 1).join(padWith) + n;
}

/** Do not remove default */
export default { getRandom, pad };
