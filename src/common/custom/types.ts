type deletedData = { ok?: number | undefined; n?: number | undefined } & {
  deletedCount?: number | undefined;
};

export { deletedData };
