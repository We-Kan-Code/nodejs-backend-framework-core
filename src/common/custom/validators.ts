import {
  registerDecorator,
  ValidationOptions,
  ValidationArguments,
  IsNumber,
} from 'class-validator';

function IsLatLong(
  property: string,
  validationOptions?: ValidationOptions,
): any {
  return (object: Object, propertyName: string): any => {
    registerDecorator({
      name: 'isLatLong',
      target: object.constructor,
      propertyName,
      constraints: [property],
      options: validationOptions,
      validator: {
        validate(value: any, args: ValidationArguments): any {
          if (Array.isArray(value) && value.length > 1) {
            /**
             * value[0] = Latitude
             * value[1] = Longitude
             */
            return (
              IsNumber(value[0]) &&
              IsNumber(value[1]) &&
              value[0] > -90 &&
              value[0] < 90 &&
              value[1] > -180 &&
              value[1] < 180
            );
          }
          return false;
        },
      },
    });
  };
}

export { IsLatLong };
