import { ValidateNested, IsNotEmpty, IsInt, Min, Max } from 'class-validator';
import { messages } from 'resources/strings/configuration/authentication';

class Token {
  @IsNotEmpty({ message: messages.validation.token.name.empty })
  public name!: string;

  @IsNotEmpty({ message: messages.validation.token.secret.empty })
  public secret!: string;

  @IsNotEmpty({ message: messages.validation.token.expiry.empty })
  @IsInt({ message: messages.validation.token.expiry.invalid })
  @Min(1, { message: messages.validation.token.expiry.invalid })
  @Max(60, { message: messages.validation.token.expiry.invalid })
  public expiry!: number;
}

class Authentication {
  @ValidateNested()
  public token!: Token;
}

export { Authentication, Token };
