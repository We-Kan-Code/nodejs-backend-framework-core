// import bunyan from 'bunyan';
import { logLevelString, Logger } from '@wekancompany/common';

class Log {
  public consoleLevel!: logLevelString;
  public consoleTo!: NodeJS.WriteStream;
  public fileLevel!: logLevelString;
  public fileTo!: string;
  public logger!: Logger;
}

export { Log };
