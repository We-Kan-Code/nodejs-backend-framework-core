import { IsDefined } from 'class-validator';
import { messages } from 'resources/strings/configuration/messaging';

class Sendgrid {
  @IsDefined({ message: messages.validation.sendgrid.api.empty })
  public apiKey!: string;
}

export { Sendgrid };
