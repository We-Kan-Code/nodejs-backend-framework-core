import {
  IsDefined,
  IsEnum,
  Min,
  validate,
  ValidateNested,
} from 'class-validator';
import { validEnvironments as validEnvs, logLevels } from 'common/enums';
import {
  InternalServerError,
  ValidationError,
  Logger,
} from '@wekancompany/common';
import { Authentication, Token } from './classes/authentication';
import { Aws, SecretsManager } from './classes/aws';
import { Database, MongoDB } from './classes/database';
import { Log } from './classes/logger';
import { messages } from 'resources/strings/configuration';
import { messages as awsMessages } from 'resources/strings/configuration/aws';
import { messages as loggerMessages } from 'resources/strings/configuration/logger';
import { Sendgrid } from './classes/messaging';
import bunyan from 'bunyan';
import dotenv from 'dotenv';
import firebase from 'firebase-admin';

class Configuration {
  public name: string = 'app';

  @Min(1025, { message: messages.errors.validation.port.invalid })
  public port: number = 9000;

  @IsEnum(validEnvs, { message: messages.errors.validation.env.invalid })
  public env: string = 'development';

  @IsDefined({ message: 'Please provide a' })
  public cors = {
    whitelist: ['http://localhost:' + this.port],
  };

  @ValidateNested()
  public authentication!: Authentication;

  @ValidateNested()
  public database!: Database;

  @ValidateNested()
  public sendgrid!: Sendgrid;

  @ValidateNested()
  public log!: Log;

  @IsDefined({ message: 'Please provide a 1' })
  public firebaseCredentials?: any;

  @ValidateNested()
  public aws!: Aws;

  public static parse(errors: any): any {
    if (errors.constraints !== undefined) {
      const constraints = Object.values(errors.constraints);
      /** Class validator reverses validation order -_- */
      const message: string = constraints[constraints.length - 1] as string;
      return new ValidationError('Configuration not valid', message);
    }
    if (errors.children !== undefined) {
      return Configuration.parse(errors.children[0]);
    }
    return new InternalServerError(
      '[Validator] Error object does not contain any constraints or children',
    );
  }

  /**
   * @description
   * @date 2019-09-08
   * @returns {Promise<{}>}
   * @memberof Configuration
   */
  public async isValid(): Promise<any> {
    return new Promise((resolve, reject): void => {
      validate(this).then((errors: any): void => {
        // errors is an array of validation errors
        if (errors.length > 0) {
          reject(Configuration.parse(errors[0]));
        } else {
          resolve('Success');
        }
      });
    });
  }

  constructor() {
    this.authentication = new Authentication();
    this.database = new Database();
    this.aws = new Aws();
  }

  /**
   * @description
   * @date 2019-09-08
   * @returns {Promise<string>}
   * @memberof Configuration
   */
  public async init(): Promise<string> {
    if (dotenv.config().error)
      throw new Error(messages.errors.validation.configuration.missing);

    const token = new Token();
    const mongodb = new MongoDB();
    const secretsManager = new SecretsManager();
    const log = new Log();
    const sendgrid = new Sendgrid();

    this.name = process.env.NAME || this.name;

    secretsManager.region = process.env.AWS_SECRETS_REGION || '';
    secretsManager.secretId = process.env.AWS_SECRETS_NAME || '';

    if (secretsManager.region === '')
      throw new Error(awsMessages.validation.secrets.region.empty);

    if (secretsManager.secretId === '')
      throw new Error(awsMessages.validation.secrets.name.empty);

    this.aws.secrets = secretsManager;

    const secrets: any = await SecretsManager.getSecrets(
      this.aws.secrets.region,
      this.aws.secrets.secretId,
    );

    token.name = process.env.AUTH_TOKEN_NAME || 'auth';
    token.expiry = Number(process.env.AUTH_TOKEN_EXPIRY);
    token.secret = secrets[0];
    this.authentication.token = token;

    this.port = Number(process.env.PORT) || this.port;
    this.env = process.env.NODE_ENV || this.env;

    this.cors.whitelist =
      String(process.env.CORS_WHITELIST)
        .trim()
        .split(',') || this.cors;

    mongodb.host = process.env.MONGODB_HOST || '';
    mongodb.database = process.env.MONGODB_DATABASE || '';
    this.database.mongodb = mongodb;

    /** Configure logging  */
    this.log = log;

    let level = process.env.LOG_CONSOLE_LEVEL || 'trace';
    if (!(level in logLevels))
      throw new Error(loggerMessages.validation.levels.invalid);
    this.log.consoleLevel = logLevels[level];
    this.log.consoleTo =
      process.env.LOG_CONSOLE_TO === 'stderr' ? process.stderr : process.stdout;

    level = process.env.LOG_FILE_LEVEL || 'debug';
    if (!(level in logLevels))
      throw new Error(loggerMessages.validation.levels.invalid);
    this.log.consoleLevel = logLevels[level];
    this.log.fileTo =
      process.env.LOG_FILE_TO || 'storage/logs/core-' + new Date() + '.log';

    const logger = await Logger.init(
      this.name,
      [
        {
          level: this.log.consoleLevel,
          stream: this.log.consoleTo,
        },
        {
          level: this.log.fileLevel,
          path: this.log.fileTo,
        },
      ],
      bunyan.stdSerializers,
    );

    if (!(logger instanceof bunyan))
      throw new Error(loggerMessages.init.failed);

    this.log.logger = new Logger(logger);

    this.sendgrid = sendgrid;
    this.sendgrid.apiKey = secrets[1];

    this.firebaseCredentials = secrets[2];

    const firebaseDatabaseUrl = process.env.FIREBASE_DATABASE || '';

    if (firebaseDatabaseUrl === '') {
      throw new Error(loggerMessages.init.failed);
    }

    firebase.initializeApp({
      credential: firebase.credential.cert(this.firebaseCredentials),
      databaseURL: firebaseDatabaseUrl,
    });

    return 'Success';
  }
}

const configuration = new Configuration();

export default configuration;
