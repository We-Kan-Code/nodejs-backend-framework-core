import configuration from 'configuration/manager';
import mongoose from 'mongoose';

class Database {
  public static connect(): Promise<{}> {
    /**
     * Options for mongodb connection
     * https://mongoosejs.com/docs/deprecations.html
     */
    const options = {
      useNewUrlParser: true,
      useCreateIndex: true,
      useFindAndModify: false,
      useUnifiedTopology: true,
    };
    const log = configuration.log.logger;
    const databaseUri =
      configuration.database.mongodb.host +
      configuration.database.mongodb.database;

    return new Promise((resolve, reject): void => {
      mongoose.connect(databaseUri, options);
      mongoose.connection.on('open', (): void => {
        resolve('Success');
      });
      mongoose.connection.on('error', (err): void => {
        reject(err);
      });
      mongoose.connection.on('disconnected', (): void => {
        log.warn('[WARN] MongoDB connection disconnected');
        mongoose.connect(databaseUri, options);
      });
      mongoose.connection.on('reconnected', (): void => {
        log.info('[INFO] MongoDB reconnected!');
      });
      process.on('SIGINT', (): void => {
        mongoose.connection.close((): void => {
          log.fatal('[FATAL] MongoDB connection closed');
          process.exit(0);
        });
      });
    });
  }
}

export default Database;
