const messages: any = {
  errors: {
    validate: {
      firstName: {
        empty: 'Please provide a first name',
        minLength: 'First name cannot be a single character',
        maxLength: 'First name cannot be more than $constraint1 characters',
      },
      lastName: {
        empty: 'Please provide a last name',
        maxLength: 'Last name cannot be more than $constraint1 characters',
      },
      email: {
        empty: 'Please provide an email address',
        invalid: 'Please provide a valid email address',
      },
      password: {
        empty: 'Please provide a password hash',
        invalid: 'Please provide a valid password hash',
      },
      profileImage: {
        invalid: 'Please provide a valid url for the profile image',
      },
      sort: {
        invalid: 'Please provide a valid sort value',
      },
    },
    create: 'User not created',
    find: 'User not found',
  },
};

export { messages };
