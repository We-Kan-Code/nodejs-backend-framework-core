const messages: any = {
  errors: {
    validate: {
      sort: {
        invalid: 'Please provide a valid sort value',
      },
    },
  },
};

export { messages };
