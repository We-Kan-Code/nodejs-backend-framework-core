const messages: any = {
  errors: {
    validate: {
      email: {
        empty: 'Please provide an email address',
        invalid: 'Please provide a valid email address',
      },
      password: {
        empty: 'Please provide a password hash',
        invalid: 'Please provide a valid password hash',
      },
    },
    generate: {
      token: {
        failed: 'Failed to generate tokens. Please try again later',
      },
      salt: {
        failed: 'Failed to generate password hash. Please try again later',
      },
    },
    unauthorized: 'Please provide a valid email / password',
  },
};

export { messages };
