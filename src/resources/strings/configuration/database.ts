const prefix = '[Configuration]';

const messages: any = {
  validation: {
    database: {
      name: {
        empty: `${prefix} Please provide the database host name`,
        valid: `${prefix} Please provide a valid database host name`,
      },
      host: {
        empty: `${prefix} Please provide the database name`,
        valid: `${prefix} Please provide a valid database name`,
      },
    },
  },
};

export { prefix, messages };
