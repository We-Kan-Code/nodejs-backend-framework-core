const prefix = '[Configuration]';

const messages: any = {
  validation: {
    token: {
      name: {
        empty: `${prefix} Please provide a name for the authentication token`,
      },
      secret: {
        empty: `${prefix} Please provide a secret key for signing authentication tokens`,
      },
      expiry: {
        empty: `${prefix} Please provide an expiry time (in minutes) for the authentication token`,
        invalid: `${prefix} Please provide an expiry time between 1 and 60 minutes for the authentication token`,
      },
    },
  },
};

export { prefix, messages };
