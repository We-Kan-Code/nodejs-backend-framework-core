const prefix = '[Configuration]';

const messages: any = {
  validation: {
    secrets: {
      region: {
        empty: `${prefix} Please provide a region for aws secrets`,
      },
      name: {
        empty: `${prefix} Please provide a aws secrets secret name`,
      },
    },
  },
};

export { prefix, messages };
