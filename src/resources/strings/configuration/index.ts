const prefix = '[Configuration]';

const messages: any = {
  errors: {
    validation: {
      port: {
        invalid: `${prefix} Please provide a valid port number (Must be greater than 1024)`,
      },
      env: {
        invalid: `${prefix} Please provide a valid value for NODE_ENV (development, production)`,
      },
      configuration: {
        missing: `.env file is missing`,
      },
    },
  },
};

export { prefix, messages };
