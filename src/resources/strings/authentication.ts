const messages: any = {
  errors: {
    validate: {
      email: {
        empty: 'Please provide an email address',
        invalid: 'Please provide a valid email address',
      },
      password: {
        empty: 'Please provide a password hash',
        invalid: 'Please provide a valid password hash',
      },
    },
    failed: {},
  },
};

export default messages;
