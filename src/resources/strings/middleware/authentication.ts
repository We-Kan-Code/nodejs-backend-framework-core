const prefix = '[Authentication]';

const messages: any = {
  login: {
    log: {
      failed: `${prefix} Login failed - `,
    },
    response: {
      failed: 'Login failed.',
      notVerified: 'Please verify your account before logging in',
    },
  },
  token: {
    log: {
      expired: 'Token expired',
      invalid: 'Invalid token',
    },
    response: {
      empty: 'Please provide an authentication token',
      invalid: 'Please provide a valid authentication token',
      expired: 'Authentication token has expired. Please login again',
    },
  },
};

export { prefix, messages };
