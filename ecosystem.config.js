module.exports = {
  apps: [
    {
      name: 'API',
      script: 'app.js',

      // Options reference: https://pm2.io/doc/en/runtime/reference/ecosystem-file/
      args: 'one two',
      instances: 1,
      autorestart: true,
      watch: false,
      max_memory_restart: '1G',
      env: {
        NODE_ENV: 'development',
      },
      env_production: {
        NODE_ENV: 'production',
      },
    },
  ],
};

// module.exports = {
//   apps: [{
//     name: 'app',
//     script: 'dist/app.js',
//     instances: "max",
//     autorestart: true,
//     watch: 'dist/**/*.js',
//     max_memory_restart: '1G',
//     env: {
//       NODE_ENV: 'development'
//     },
//     env_production: {
//       NODE_ENV: 'production'
//     }
//   },
//   {
//     name: 'app-watcher',
//     script: 'npm start',
//     instances: 1,
//     autorestart: true,
//     watch: 'tsconfig.json',
//     env: {
//       NODE_ENV: 'development'
//     },
//     env_production: {
//       NODE_ENV: 'production'
//     }
//   }]
// };
