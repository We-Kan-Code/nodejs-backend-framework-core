declare module '@wekancompany/common' {
  import bunyan from 'bunyan';

  export class Logger {
    private logger: bunyan;
    static init(
      name: string,
      streams: bunyan.Stream[],
      serializers: bunyan.Serializers,
    ): Promise<bunyan>;
    constructor(logger: bunyan);
    trace(trace: any): Promise<void>;
    debug(debug: any): Promise<void>;
    info(info: any): Promise<void>;
    warn(warning: any): Promise<void>;
    error(error: any): Promise<void>;
    fatal(fatal: any): Promise<void>;
    static logErrorsWithFallback(
      log: any,
      prefix: string,
      error: any,
    ): Promise<void>;
  }

  export type logLevelString =
    | number
    | 'trace'
    | 'debug'
    | 'info'
    | 'warn'
    | 'error'
    | 'fatal'
    | undefined;
  export const apiErrorCodes: Map<number, string>;
  export const enum httpStatus {
    ok = 200,
    created = 201,
    accepted = 202,
    noContent = 204,
    resetContent = 205,
    notModified = 304,
    badRequest = 400,
    unauthorized = 401,
    forbidden = 403,
    notFound = 404,
    methodNotAllowed = 405,
    notAcceptable = 406,
    conflict = 409,
    lengthRequired = 411,
    preconditionFailed = 412,
    payloadTooLarge = 413,
    uriTooLong = 414,
    unsupportedMediaType = 415,
    unprocessableEntity = 422,
    preconditionRequired = 428,
    tooManyRequests = 429,
    requestHeaderFieldsTooLarge = 431,
    internalServerError = 500,
    notImplemented = 501,
  }

  export class ApiError {
    name: string;
    message: string;
    errors: {};
    status: number;
    constructor(status: number, message: string, errors?: {});
  }
  export class BadRequestError extends ApiError {
    constructor(message: string, errors?: {});
  }
  export class UnauthorizedError extends ApiError {
    constructor(message: string, errors?: {});
  }
  export class ForbiddenError extends ApiError {
    constructor(message: string, errors?: {});
  }
  export class NotFoundError extends ApiError {
    constructor(message: string, errors?: {});
  }
  export class NotAcceptableError extends ApiError {
    constructor(message: string, errors?: {});
  }
  export class ConflictError extends ApiError {
    constructor(message: string, errors?: {});
  }
  export class ValidationError extends ApiError {
    constructor(message: string, errors?: {});
  }
  export class InternalServerError extends ApiError {
    constructor(message: string, errors?: {});
  }
}
