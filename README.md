# Backend Framework

## Description

A framework for building Node.js server-side applications. 

Under the hood, the framework makes use of Express. As a result, it supports a wide range of third-party plugins.

## Getting started

Check out our [guide](https://bitbucket.org/We-Kan-Code/nodejs-backend-framework-core/wiki/). Visit https://bitbucket.org/We-Kan-Code/nodejs-backend-framework-core/wiki/

